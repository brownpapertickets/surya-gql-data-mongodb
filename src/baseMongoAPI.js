import { MongoQuery } from "./mongoQuery"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"
import { BaseCRUDAPI } from "@brownpapertickets/surya-gql-data"
import mongoose from "mongoose"

const DEFAULT_LIMIT = 100

export class BaseMongoAPI extends BaseCRUDAPI {
  constructor(container, type, typeMap, config) {
    super(container, type, typeMap, config)
    this.log.info(
      `BaseMongoAPI construct. type:${this.getTypeName()} config: ${JSON.stringify(
        config
      )}`
    )
    this.mongoQuery = new MongoQuery(container, type, typeMap)
  }

  getCollectionName() {
    return this.config.collection || this.getTypeName()
  }

  getCollection() {
    return this.db.collections[this.getCollectionName()]
  }

  // ====================================================
  // Generate a mongoose schema definition from the associated Type specification.
  toMongooseSchema(typeMap) {
    const typeSpec = this.type.typeSpec()
    let options = {
      id: false,
    }
    if (!!typeSpec.timestamps) {
      options["timestamps"] = {
        createdAt: "createdDate",
        updatedAt: "updatedDate",
      }
    }

    const schemaDef = this._toMongooseSchema(typeSpec, typeMap)
    return { schemaDef, options }
  }

  _toMongooseSchema(typeSpec, typeMap) {
    let schema = {}
    const fields = typeSpec.fields
    const fieldNames = Object.keys(fields)
    fieldNames.forEach((fieldName) => {
      const fieldSpec = fields[fieldName]
      const { altName, fieldDef } = this._genFieldDef(fieldSpec, typeMap)
      const useName = altName ? altName : fieldName
      schema[useName] = fieldDef
    })
    return schema
  }

  _genFieldDef(fieldSpec, typeMap) {
    let fieldDef = {}
    let altName = null
    const isId = !!fieldSpec.key

    if (isId) {
      // ids are always named _id and type is always ID
      altName = "_id"
      fieldDef = {
        type: mongoose.Types.ObjectId,
        required: true,
        auto: true,
      }
    } else {
      altName = fieldSpec.asName || null
      let mongooseType = String
      const required = !!fieldSpec.required
      const unique = !!fieldSpec.unique
      const index = !!fieldSpec.index
      let defaultValue = undefined
      if (fieldSpec.defaultValue != undefined) {
        defaultValue = fieldSpec.defaultValue
      }

      const typeName = fieldSpec.baseType || fieldSpec.type // Allow spec of baseType for custom Scalars
      switch (typeName) {
        case ScalarTypes.String:
          mongooseType = String
          break
        case ScalarTypes.ID:
          mongooseType = mongoose.Types.ObjectId
          break
        case ScalarTypes.Integer:
          mongooseType = Number
          break
        case ScalarTypes.Float:
          mongooseType = Number
          break
        case ScalarTypes.DateTime:
          mongooseType = Date
          break
        case ScalarTypes.Boolean:
          mongooseType = Boolean
          break
        case ScalarTypes.Object:
          mongooseType = this._genSubSchema(fieldSpec.objectType, typeMap)
          break
        case ScalarTypes.ObjectRef:
          mongooseType = mongoose.Types.ObjectId
          altName = fieldSpec.refField
          break
      }
      const isArray = !!fieldSpec.array
      if (isArray) {
        fieldDef["type"] = [mongooseType]
      } else {
        fieldDef["type"] = mongooseType
      }
      if (required) {
        fieldDef["required"] = true
      }
      if (index) {
        fieldDef["index"] = true
      }
      if (unique) {
        fieldDef["unique"] = true
      }
      if (defaultValue != undefined) {
        if (defaultValue == "$NOW") {
          defaultValue = Date.now
        }
        fieldDef["default"] = defaultValue
      }
    }

    return { altName, fieldDef }
  }

  _genSubSchema(typeName, typeMap) {
    const typeSpec = typeMap[typeName]
    let mongooseDef = this._toMongooseSchema(typeSpec, typeMap)
    const schema = new mongoose.Schema(mongooseDef, { id: false })
    return schema
  }

  // CRUD Methods -----------------------------
  async query(
    offset = 0,
    limit = DEFAULT_LIMIT,
    filter = "",
    sort = "",
    session
  ) {
    const col = this.getCollection()
    const mongoFindSpec = this.mongoQuery.queryFilter(filter)
    const mongoSortSpec = this.mongoQuery.querySort(sort)
    const total = await col.countDocuments(mongoFindSpec)
    const query = col
      .find(mongoFindSpec)
      .skip(offset)
      .limit(limit)
      .sort(mongoSortSpec)

    let data
    if (session) {
      data = await query.exec().session(session)
    } else {
      data = await query.exec()
    }

    let items = data.map((doc) => {
      return Object.create(this.type).loadDocument(doc)
    })
    const result = {
      items: items,
      total: total,
      count: items.length,
      offset: offset,
    }

    return result
  }

  async get(id, session) {
    const col = this.getCollection()
    let doc
    if (session) {
      doc = await col.findById(id).session(session)
    } else {
      doc = await col.findById(id)
    }

    if (doc) {
      return Object.create(this.type).loadDocument(doc)
    }
    return null
  }

  async create(data, session) {
    let col = this.getCollection()
    let typeObj = Object.create(this.type).loadData(data)
    const nativeObj = typeObj.toNativeJson()
    let savedDoc = {}
    try {
      if (session) {
        const result = await col.create([nativeObj], { session: session })
        savedDoc = result[0]
      } else {
        savedDoc = await col.create(nativeObj)
      }
    } catch (ex) {
      this.log.error(`Error saving: ${ex.message}`)
      throw ex
    }

    return Object.create(this.type).loadDocument(savedDoc)
  }

  async update(id, data, session) {
    const col = this.getCollection()
    let typeObj = Object.create(this.type).loadData(data)
    const nativeObj = typeObj.toNativeJson()
    let savedDoc = {}
    try {
      if (session) {
        savedDoc = await col
          .findOneAndUpdate({ _id: id }, nativeObj, {
            new: true,
          })
          .session(session)
      } else {
        savedDoc = await col.findOneAndUpdate({ _id: id }, nativeObj, {
          new: true,
        })
      }
    } catch (ex) {
      this.log.error(`Error updating: ${ex.message}`)
      throw ex
    }
    return Object.create(this.type).loadDocument(savedDoc)
  }

  async delete(id, session) {
    const col = this.getCollection()
    let deletedDoc = {}
    try {
      if (session) {
        deletedDoc = await col.findOneAndDelete({ _id: id }).session(session)
      } else {
        deletedDoc = await col.findOneAndDelete({ _id: id })
      }
    } catch (ex) {
      this.log.error(`Error deleting: ${ex.message}`)
      throw ex
    }
    return Object.create(this.type).loadDocument(deletedDoc)
  }
}
