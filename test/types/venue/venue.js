import { BaseType } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class Venue extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Venue",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          key: true,
          required: true,
          input: false,
        },
        name: {
          type: ScalarTypes.String,
          required: true,
        },
        venueActive: {
          type: ScalarTypes.Boolean,
          asName: "active",
        },
        address: {
          type: ScalarTypes.Object,
          objectType: "Address",
        },
        owner: {
          type: ScalarTypes.ObjectRef,
          objectType: "User",
          refField: "ownerId",
          refFieldType: ScalarTypes.ID,
          ownerId: true,
        },
        createdDate: {
          type: ScalarTypes.DateTime,
        },
        updatedDate: {
          type: ScalarTypes.DateTime,
        },
      },
    }
  }
}
