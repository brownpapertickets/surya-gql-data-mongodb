import { AddressAPI } from "./types/address/addressAPI"
import { Address } from "./types/address/address"
import { MockLog } from "./mockLog"
import { mongo } from "mongoose"

let container
beforeAll(() => {
  container = { log: new MockLog() }
})

describe("mongoAPI Tests", () => {
  test("Test mongoose Schema Gen: Address", async () => {
    const address = new Address()
    const addressAPI = new AddressAPI(container, address, [address], {
      collection: "address",
    })
    const mongooseSchema = addressAPI.toMongooseSchema([address])
    const schemaDef = mongooseSchema.schemaDef
    // console.log(JSON.stringify(schemaDef, null, 2))
    expect(schemaDef).toEqual({
      address1: { type: String },
      address2: { type: String },
      country: {
        type: String,
        default: "US",
      },
      postal_code: { type: String, unique: true },
      state: { type: String },
      city: { type: String },
      addressType: { type: String },
      date_created: { type: Date, default: Date.now },
      default_address: { type: Boolean, default: false },
    })
  })
})
