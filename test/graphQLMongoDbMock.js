import { MongoAPIMock } from "./mongoAPIMock"

export class GraphQLMongoDbMock {
  constructor(container) {
    this.container = container
  }
  async connect(mongoUri, isProduction) {
    return this
  }
  getAPI(modelName) {
    return new MongoAPIMock(this.container, null)
  }

  newObjectId(s) {
    return null
  }

  async disconnect() {}
}
